const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];
// export function validate(mail) {
function validate(mail) {
    let splited = mail.split("@")[1];
    let isValid = VALID_EMAIL_ENDINGS.includes(splited) ? true  : false;
    console.log(isValid);
};


const sendHttpRequest = (method, url, data) => {
    return fetch(url, {
        method: method,
        body: JSON.stringify(data),
        headers: data ? {
            'Content-Type': 'application/json'
        } : {}
    }).then(response => {
        if (response.status > 422) {
            return response.json().then(errResData => {
                const error = new Error('Something went wrong!');
                error.data = errResData;
                window.alert(error)
            });
        }
        return response.json();
    });
};
export {validate, sendHttpRequest}