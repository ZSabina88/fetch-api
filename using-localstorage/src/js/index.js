import { SectionCreator} from "./join-us-section.js";
import { sendHttpRequest } from "./email-validator.js";
import css from '../styles/style.css'assert { type: 'css' };

document.adoptedStyleSheets = [css];
const sectionCreator = new SectionCreator();

const standardSection = sectionCreator.create("Standard");



const communitySection = document.querySelector("app-cards__children");
// const communitySection = document.querySelector("app-cards");
document.onload = sendHttpRequest('GET', '/community', {
    communitySection
}).then(responseData => {
    responseData.forEach(card => {
        communitySection.innerHTML +=
            `<img src="${card.avatar}">
            <div>
            <h5>${card.firtsName}${card.lastNAme}</h5>
            <p>${card.position}</p>
            </div> `
        });
        
    }
).catch(err => {
    console.log(err, err.data);
});


// async function getData (){
//     const response= await fetch('/community')
//     .then((response) => {
//         // if (!response.ok) {
//         //   throw new Error(HTTP error: ${response.status});
//         // }
//         return response.json();
//       }).then(responseData => {
//         let community = "";
//             responseData.map((card) => {
//                 community +=
//                 `<img src="${card.avatar}">
//                     <div>
//                     <h5>${card.firtsName}${card.lastNAme}</h5>
//                     <p>${card.position}</p>
//                     </div> `
//                 });
//                 document.querySelector(".app-cards").innerHTML = community;
//             })
//         //     .catch(err => {
//         //     console.log(err, err.data);
//         // });
        
// }

// getData();


// async function getData (){
//     const response= await fetch('/community', {
//         method: GET,
//         body: JSON.stringify(data),
//         headers: data ? {
//             'Content-Type': 'application/json',
//             'Accept': 'application/json'
//         } : {}
//     })
//     .then((response) => {
//         if (!response.ok) {
//           throw new Error(HTTP error: ${response.status});
//         }
//         return response.json();
//       }).then(responseData => {
//         let community = "";
//             responseData.map((card) => {
//                 community +=
//                 `<img src="${card.avatar}">
//                     <div>
//                     <h5>${card.firtsName}${card.lastNAme}</h5>
//                     <p>${card.position}</p>
//                     </div> `
//                 });
//                 document.querySelector(".app-cards").innerHTML = community;
//             })
//             .catch(err => {
//             console.log(err, err.data);
//         });
        
// }

// getData();
